FROM tiangolo/uvicorn-gunicorn-starlette:python3.7
COPY ./requirements.txt /tmp/
RUN pip install -r /tmp/requirements.txt
COPY ./app /app/app