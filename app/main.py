import json
from starlette.applications import Starlette
from starlette.responses import JSONResponse
from starlette.responses import PlainTextResponse
from starlette.routing import Route
from starlette.middleware import Middleware
from starlette.middleware.cors import CORSMiddleware


documents = []

filename = 'app/assets/saved_documents.json'

with open(filename, 'r') as file:
    documents = json.loads(file.read())


async def show_documents(request):
    return JSONResponse({'documents': documents}, status_code=200)


async def post_document(request):
    data = await request.json()

    with open(filename, 'w') as file:
        for document in data['documents']:
            if document in documents:
                return JSONResponse({'message':'Bad request - this document type has already been saved!'}, status_code=400)
            if document not in documents:
                documents.append(document)

        document_indices_to_remove = []
        for i, document in enumerate(documents):
            if document not in data['documents']:
                document_indices_to_remove.append(i)

        for document_index in sorted(document_indices_to_remove, reverse=True):
            del documents[document_index]
        json.dump(documents, file)

        return JSONResponse({'documents': documents}, status_code=201)


routes = [
    Route('/', endpoint=post_document, methods=['POST']),
    Route('/', endpoint=show_documents, methods=['GET']),
]


middleware = [
    Middleware(CORSMiddleware, allow_origins=['*'], allow_methods=['GET', 'POST'])
]


app = Starlette(debug=True, routes=routes, middleware=middleware)