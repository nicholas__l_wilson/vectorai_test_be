import json
from starlette.testclient import TestClient
from app.main import app


client = TestClient(app)

def test_show():
    # test get response
    response = client.get('/')
    assert response.status_code == 200
    assert isinstance(response.json(), list)

def test_post():
    data = {'documents': ['Bill of lading', 'Commercial invoice']}
    response = client.post('/', data=json.dumps(data))
    assert response.status_code == 201
    assert isinstance(response.json(), dict)
    assert isinstance(response.json()['documents'], list)

if __name__ == "__main__":
    test_show()
    test_post()
    print('tests ok')